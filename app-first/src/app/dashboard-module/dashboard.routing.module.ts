import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {AuthGuard} from "../services/auth-guard.service";

const routes:Routes=[
  {
    path:'',
    component:LandingPageComponent,
    canActivate: [AuthGuard]
  }
]
@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})
export class DashboardRoutingModule {}
