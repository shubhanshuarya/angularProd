import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {DashboardRoutingModule} from "./dashboard.routing.module";



@NgModule({
  declarations: [LandingPageComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ],
  exports:[CommonModule]
})
export class DashboardModuleModule { }
