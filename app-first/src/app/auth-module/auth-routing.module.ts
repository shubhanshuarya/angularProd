import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthComponent} from "./auth.component";
import {LoginComponent} from "./login/login.component";
import {RegistrationComponent} from "./registration/registration.component";
import {ForgotPswdComponent} from "./forgot-pswd/forgot-pswd.component";
import {ResetPswdComponent} from "./reset-pswd/reset-pswd.component";
import {AccessGuardService} from "../services/access-guard.service";
import {VerifyOtpComponent} from "./verify-otp/verify-otp.component";

const routes:Routes=[
  {
    path:'',
    component:AuthComponent,
    children:[
      { path:'login',component:LoginComponent,canActivate: [AccessGuardService]},
      { path:'register',component:RegistrationComponent,canActivate: [AccessGuardService]},
      { path:'forgetPassword',component:ForgotPswdComponent},
      { path:'resetPassword/:token',component:ResetPswdComponent},
      {path:'verifyOtp',component:VerifyOtpComponent}
    ]
  }
]
@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})
export class AuthRoutingModule {}
