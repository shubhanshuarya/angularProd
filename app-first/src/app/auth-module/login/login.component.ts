import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import {
  GoogleLoginProvider,
  SocialAuthService,
  SocialUser,
} from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private dataService: DataService,
    private router: Router,
    private socialAuthService: SocialAuthService
  ) {
    console.log('Auth :' + this.dataService.isLogged);
  }
  user!: SocialUser;
  loggedIn!: boolean;

  ngOnInit(): void {
    // if (localStorage.getItem('auth')) {
    //   this.dataService.isLogged = false;
    //   this.router.navigate(['/profile']);
    // }
    this.socialAuthService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = user != null;
      console.log('User logged, google');
      console.log(user);
    });
  }
  showMessage: boolean = false;
  logProcessMessage: string = 'Checking Username';

  userName: string = '';
  password: string = '';

  loginWithGoogle(): void {
    this.showMessage = true;
    this.logProcessMessage = 'oAuth Logging User';

    console.log('Login with google start');
    this.socialAuthService
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((res) => {
        console.log(res);

        this.dataService
          .verifyAccessToken({ accessToken: res.authToken, email: res.email })
          .subscribe({
            next: (data: any) => {
              console.log('Data rec: from oAuth route');
              console.log(data);
              console.log(data.body.loginRedirect);
              if (data.body.loginRedirect) {
                this.logProcessMessage =
                  'oAuth Login success, redirect Profile';
                this.dataService.JWT_TOKEN = data.body.jwt;
                //updating the user mobile verification status
                this.dataService.isMobileVerified=data.body.isMobileVerified;
                this.logProcessMessage = 'User Logged in, Redirect profile :-)';
                localStorage.setItem('auth', JSON.stringify(data.body.jwt));
                this.dataService.userStatus.emit(true);
                this.router.navigate(['/dashboard']);
                this.logProcessMessage =
                  'oAuth Login success, redirect Profile';
              } else {
                this.logProcessMessage = 'Error Logging user !';
                setTimeout(() => {
                  this.showMessage = false;
                }, 3000);
              }
            },
            error: (err) => {
              console.log(err);
              console.log('error in oauth');
            },
          });
      });
  }
  callLoginUser() {
    if (this.userName=="" || this.password==""){
      this.showMessage = true;
      this.logProcessMessage = 'Please Enter Credentials!';
      setTimeout(() => {
        this.showMessage = false;
      }, 2000);
      return;
    }
    this.loginUser({
      userName: this.userName,
      pswd: this.password,
    });
  }

  loginUser(data: Object) {
    console.log('Login user ng called');
    this.showMessage = true;
    this.logProcessMessage = 'Logging User';
    this.dataService.loginUser(data).subscribe({
      next: (data: any) => {
        console.log('data recieved LOGIN');
        console.log(data);
        //storing the jwt token
        console.log(data.body.jwt);
        this.dataService.JWT_TOKEN = data.body.jwt;
        //updating the user mobile verification status
        this.dataService.isMobileVerified=data.body.isMobileVerified;

        this.logProcessMessage = 'User Logged in, Redirect profile :-)';
        //setting the auth key in local storage

        localStorage.setItem('auth', JSON.stringify(data.body.jwt));
        console.log('Setting auth key in Local storage');
        console.log(localStorage.getItem('auth'));
        this.dataService.userStatus.emit(true);
        this.router.navigate(['/dashboard']);
      },
      error: (err) => {
        this.logProcessMessage = 'Error Logging user !';
        console.log('error occured in Logging in User');
        console.log(err);
        if (err.status === 401 || err.status === 404)
          this.logProcessMessage = err.error.info;
        setTimeout(() => {
          this.showMessage = false;
        }, 2000);
      },
    });
  }
}
