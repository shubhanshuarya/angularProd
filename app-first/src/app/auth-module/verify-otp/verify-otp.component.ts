import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.css']
})
export class VerifyOtpComponent implements OnInit {
  constructor(private dataServ: DataService,
              private route: ActivatedRoute,
              private router: Router,) {}
  ngOnInit(): void {
    console.log('Verify mobile module start');
  }

  mobileNumber:string='';
  enteredOtp:string='';
  otpSent:boolean=false;
  otpVerifed:boolean=false;
  readonlyInput:boolean=false;

  otpIDforVerify:string='';

  sendOtp(){
    console.log('Send otp called')
    this.showMessage=true;
    this.logProcessMessage = 'OTP Sending wait...';
    this.dataServ.sendOtp()
      .subscribe({
        next: (data: any) => {
          console.log(data)
          this.readonlyInput=true;
          if (data.response.errorCode){
            throw Error('Error sending SMS')
          }
          this.otpSent=true;  //show field to enter otp
          this.logProcessMessage = 'OTP sent successfully, enter OTP to verify';
          console.log(data.otpID)
          this.otpIDforVerify=data.otpID;
          console.log('SMS send successful');
          console.log(data);
        },
        error: (err) => {
          console.log('Sending sms Error');
          console.log(err);
          this.logProcessMessage = 'Retry error sending OTP';
        }
      })
  }
  verifyOtp(){
    if (this.otpIDforVerify){
      this.dataServ.verifyOtp(this.enteredOtp,this.otpIDforVerify)
        .subscribe({
          next: (data: any) => {
            this.otpVerifed=true;  //show field to enter otp
            this.logProcessMessage = 'OTP Verified successfully';
            console.log(data)
            this.dataServ.isMobileVerified=true;//setting mobile verification to true

            this.router.navigate(['/dashboard'])
          },
          error: (err) => {
            console.log('Verifying sms Error');
            console.log(err);
            this.logProcessMessage = 'Retry error verifying OTP';
          }
        })
    }else {
      this.showMessage=true;
      this.logProcessMessage = 'OTP Verified, please retry';
    }
  }
  showMessage: boolean = false;
  logProcessMessage: string = 'Sending Mail wait  ';
}
