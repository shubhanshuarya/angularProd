import { Component, ElementRef, Input, OnInit, Type } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { Template1Component } from './display/template1/template1.component';
import { Template2Component } from './display/template2/template2.component';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css'],
})
export class ResumeComponent implements OnInit {
  constructor(private dataServ: DataService, private router: Router) {}

  ngOnInit(): void {
    this.loadAllResume();
    console.log('Resume component init called , loaded once again all resumes');
    this.dataServ.refreshResume.subscribe((data: any) => {
      this.loadingStatus = true;
      console.log('[Resume Comp]: Refresh request recieved');
      this.loadAllResume();
    });
    this.dataServ.templatesShow.subscribe((state) => {
      this.showTemplates = state;
    });
  }
  loadingStatus: boolean = true;
  showResume: boolean = false;
  message = 'Loading  ';
  selectedResume!: string ;
  allResume!: Array<any>; //stores all the resume

  loadAllResume() {
    this.loadingStatus = true;
    this.message = 'Loading all resume';
    this.dataServ.getAllCV().subscribe(
      (data: any) => {
        console.log('(resume:loadAll resume) All cv data recieved');
        console.log(data);
        this.allResume = data.body;
        this.loadingStatus = false;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  editResume(_id: HTMLHeadingElement) {
    this.showTemplates = false;
    this.selectedResume = _id.innerText;
    console.log(_id.innerText);
    console.log('Emitting edit resume request to the form component');
    this.dataServ.editResumeSelect.emit(_id.innerText);
  }
  previewResume(_id: HTMLHeadingElement) {
    this.selectedResume = _id.innerText;
    console.log(_id.innerText);
    console.log('Emitting preview resume to display component');
    this.dataServ.previewResumeSelect.emit(_id.innerText);
  }

  deleteResume(curId: string) {
    console.log('Current resume ID [delete]: ' + curId);
    this.dataServ.deleteCV(curId).subscribe((respo: any) => {
      console.log('Delete resume request send');
      console.log(respo);
      this.dataServ.refreshResume.next('refresh Resume');
      this.dataServ.openSnackBar('Resume Deleted Successfully', 'Ok');
    });
  }
  closeResume() {
    this.showResume = false;
  }
  openResume() {
    this.showResume = true;
  }
  public ngOnDestroy(): void {
    this.dataServ.refreshResume.unsubscribe();
  }

  //for resume template change
  templates: templates[] = [
    {
      name: 'First',
      loc: 'assets/images/tmp_01.png',
      component: Template1Component,
    },
    {
      name: 'second',
      loc: 'assets/images/tmp_02.png',
      component: Template2Component,
    },
    {
      name: 'third',
      loc: 'assets/images/tmp_03.png',
      component: Template1Component,
    },
  ];

  showTemplates: boolean = false;

  templateChange(meta: string) {
    this.selectedResume = meta;
    this.showTemplates = true;
    console.log('Change resume Design: ' + meta);
    this.dataServ.closeForm.next(false);
  }
  // public component: Type<any>

  showSelectedDesign(index: number) {
    console.log('Showing selected resume: ' + index);
    this.dataServ.templateSelect.next({
      resumeID: this.selectedResume,
      componentIndex: this.templates[index],
    });
  }
}
interface templates {
  name: string;
  loc: string;
  component: Type<any>;
}
