import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { from, fromEvent, interval, timer, combineLatest } from 'rxjs';
import {
  map,
  pluck,
  take,
  takeLast,
  takeUntil,
  withLatestFrom,
} from 'rxjs/operators';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-take',
  templateUrl: './take.component.html',
  styleUrls: ['./take.component.css'],
})
export class TakeComponent implements OnInit, AfterViewInit {
  constructor(private dataCon: DataService) {}

  ngOnInit(): void {
    const nameSource = from([
      'Johnathon  ',
      'Emery  ',
      'Sunni  ',
      'Nina  ',
      'Francina  ',
      'Kacey  ',
    ]);
    nameSource.subscribe((res) => this.dataCon.printEle(res, 'orignal'));
    //taking only 3 data names
    nameSource
      .pipe(take(3))
      .subscribe((res) => this.dataCon.printEle(res, 'one'));

    //take last
    nameSource
      .pipe(takeLast(3))
      .subscribe((res) => this.dataCon.printEle(res, 'two'));

    const emitSource = interval(1000);
    const fiveSecTimer = timer(5000);
    // @ts-ignore
    const condit2 = fromEvent(document.getElementById('stop'), 'click');
    //take until
    emitSource
      .pipe(
        map((res: any) => 'Number ' + res),
        takeUntil(fiveSecTimer)
      )
      .subscribe((res) => this.dataCon.printEle(res, 'three'));

    //to implement the combine Latest and with latest from operator
  }
  names: string[] = [
    'avinash',
    'Tarun',
    'Rahul',
    'Yogesh',
    'Pradeep',
    'Ankush',
  ];
  colors: string[] = ['red', 'yellow', 'green', 'skyblue', 'pink', 'orange'];
  @ViewChild('nameTemp')
  curName!: ElementRef;
  @ViewChild('colorTemp')
  curColor!: ElementRef;

  ngAfterViewInit(): void {
    // fromEvent(this.curName.nativeElement, 'change').subscribe((res: any) =>
    //   console.log(res.target.value)
    // );

    let nameObs = fromEvent(this.curName.nativeElement, 'change').pipe(
      pluck('target', 'value')
    );
    let colorObs = fromEvent(this.curColor.nativeElement, 'change').pipe(
      pluck('target', 'value')
    );

    //combine latest
    combineLatest([nameObs, colorObs]).subscribe(([naam, col]: any) => {
      console.log('Combine Latest' + naam + ' ' + col);
      this.createBox(naam, col, 'combineLatestResult');
    });

    //with latest from
    nameObs.pipe(withLatestFrom(colorObs)).subscribe(([name, color]: any) => {
      this.createBox(name, color, 'withLatestResult');
    });
  }

  //create box
  createBox(name: string, color: string, container: string) {
    let nayaele = document.createElement('div');
    nayaele.innerText = name;
    nayaele.style.backgroundColor = color;
    nayaele.style.padding = '10px';
    nayaele.style.fontSize = '20px';
    nayaele.style.margin = '5px';
    document.getElementById(container)?.appendChild(nayaele);
  }
}
