import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  constructor(private dataService: DataService, private router: Router) {
    this.dataService.userStatus.subscribe((status: boolean) => {
      console.log('User log status change: ' + status);
      this.userLogged = status;
    });
  }

  ngOnInit(): void {
    this.userLogged = this.dataService.isLogged;
    this.dataService.dataChannel.subscribe((data: any) => {
      console.log('Nav Bar: Data recieved form next');
      console.log(data);
    });
  }
  //---------Code for assignments------------------
  userLogged: boolean = false;

  //---------Code for assignments------------------

  //here we are emitting the selected bar to display, which will be handled by the app component
  @Output() barSelected = new EventEmitter<string>();

  selectBar(feature: string) {
    this.barSelected.emit(feature);
  }

  logoutUser() {
    console.log('Logging OUT user');
    this.dataService.logoutUser().subscribe({
      next: (data) => {
        console.log('Logged out user');
        console.log(data);
        localStorage.clear(); //deleting the user in local storage
        this.userLogged = false;
        this.dataService.isLogged = false;
        this.dataService.isMobileVerified=false;//setting mobile verification to true
        this.dataService.userStatus.emit(false);
        this.router.navigate(['/auth/login']);
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
}
