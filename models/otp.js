const mongoose = require("mongoose");
const token = mongoose.Schema(
    {
        userName: {
            type: String,
        },
        expiry: {
            type: Date,
        },
        otp:{
            type:String
        }
    },
    { collection: "opt" }
);
module.exports = mongoose.model("otp", token);
