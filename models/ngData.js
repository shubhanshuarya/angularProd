var mongoose = require("mongoose");

var ngData = mongoose.Schema(
  {
    fname: {
      type: String,
    },
    lname: {
      type: String,
    },
    userName: {
      type: String,
      unique: true,
    },
    email: {
      type: String,
    },
    isOauth: {
      type: Boolean,
      default: false,
    },
    pswd: {
      type: String,
    },
  mobileVerified:{
    type:Boolean,
      default:false
  }
  },
  { collection: "ngData" }
);

// Compile model from schema
module.exports = mongoose.model("ngData", ngData);
